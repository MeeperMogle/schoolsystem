# Installation of SchoolSystem


## Step 1: Setting up a database connection resource (in Glassfish)

#### Derby
Run, in order, from command line:
```
asadmin
create-jdbc-connection-pool --datasourceclassname org.apache.derby.jdbc.ClientDataSource --restype javax.sql.XADataSource --property portNumber=1527:password=jonasDan:user=jonasDan:serverName=localhost:databaseName=jonasDan_database:connectionAttributes=\;create\\=true jonas_dan_uppgift_pool
create-jdbc-resource --connectionpoolid jonas_dan_uppgift_pool jdbc/jpa_jonasDan_u
```

#### PostgreSQL
NOTE: PostgreSQL forces lowercase username and database name.
    Therefore camelCase is not used in username or database name.

- Put PostgreSQL driver jar in glassfish\domains\domain1\lib\
- Can be found on `https://jdbc.postgresql.org/`

Restart Glassfish to load the driver;
```
asadmin stop-domain
asadmin start-domain
```

Set up a PostgreSQL user and database through command window:
```
psql -U postgres -h localhost
CREATE USER jonasdan WITH PASSWORD 'jonasDan';
CREATE DATABASE jonasdan_database WITH OWNER jonasdan ENCODING 'UTF8';
\q
```

- Set up a connection pool & resource for Glassfish:
```
asadmin
create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGSimpleDataSource --restype javax.sql.XADataSource --property portNumber=5432:password=jonasDan:user=jonasdan:serverName=localhost:databaseName=jonasdan_database jonas_dan_uppgift_pool
create-jdbc-resource --connectionpoolid jonas_dan_uppgift_pool jdbc/jpa_jonasDan_u
```


## Step 2: Link project to database resource

Add `jdbc/jpa_jonasDan_u` inside the `<jta-data-source>` tag of `src/main/resources/META-INF/persistence.xml`

Note: In production it is wise to remove `<property name="javax.persistence.schema-generation.database.action" value="create"/>` from `persistence.xml`

## Step 3: Adding default & dummy data to database

Note: port-number is typically 8080, may vary on your system.

- Visit `http://localhost:<port-number>/SchoolSystem/dummy.xhtml`
- Click Go
- Note: This file should not be accessible in production

This will add several users, courses and more to the database. Username:`Password` (Role)

- helena:`helena123` (Administrator)
- daniel:`daniel123` (Teacher)
- bengt1:`bengt123` (Student)
- bengt2:`bengt123` (Teacher)
- gino:`gino123` (Student)

## Step 4: Site capabilities

Note: port-number is typically 8080, may vary on your system.

Website is typically available on `http://localhost:<port-number>/SchoolSystem/` 

- Administrator can add/edit/delete: Teacher, Student, Course. De/register students on courses. View all available attendance information.
- Teacher can add/edit/delete: Lessons connected to their Courses. Add and view attendance statistics for their Courses.
- Student can de/register themselves on courses. View their own attendance statistics.