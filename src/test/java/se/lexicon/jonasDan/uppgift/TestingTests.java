package se.lexicon.jonasDan.uppgift;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import se.lexicon.jonasDan.uppgift.beans.Navigator;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Jonas Olsson (2016-09-08)
 */
public class TestingTests {

    @Test
    @DisplayName("My 1st JUnit 5 test! 😎")
    void myFirstTest(TestInfo testInfo) {
        System.out.println(Navigator.navigate("student/show"));
    }
}
