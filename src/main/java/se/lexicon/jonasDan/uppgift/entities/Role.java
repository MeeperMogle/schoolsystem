package se.lexicon.jonasDan.uppgift.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-08)
 */
@Entity
@NamedQueries({
        // Returns all Roles - should this be possible somewhere?
        @NamedQuery(name = "GetAllRoles",
                query = "SELECT r FROM Role r"),
        @NamedQuery(name = "FindRoleByRoleName",
                query = "SELECT r FROM Role r WHERE r.name = :name")
})
public class Role {

    public static enum NAMED_QUERIES {GetAllRoles, FindRoleByRoleName}

    @Id
    @GeneratedValue
    private Long id;

    @Size(min = 5, max = 20, message = "Role name length should be 5-20")
    @NotNull
    @Column(unique = true)
    private String name;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param roleList list of Role's to check for a certain one
     * @param name String name of the role; case ignored
     * @return
     */
    public static boolean containsRoleWithName(List<Role> roleList, String name) {
        for (Role currentRole : roleList) {
            if (currentRole.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Role:" + this.getName();
    }
}
