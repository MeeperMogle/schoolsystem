package se.lexicon.jonasDan.uppgift.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-08)
 */


@Entity
@NamedQueries({
        // Returns all Lessons
        @NamedQuery(name = "GetAllLessons",
                query = "SELECT cd FROM Lesson cd"),
        // Returns all Courses at a specific day
        @NamedQuery(name = "GetLessonsByCourseAndDay",
                query = "SELECT c FROM Lesson cD, Course c WHERE lower(cD.date) = lower(:date)" +
                        "AND c = cD.course"),
        // Returns all Courses at a specific day
        @NamedQuery(name = "GetLessonsByCourseId",
                query = "SELECT l FROM Lesson l WHERE l.course.id = :id"),
        // Returns Lesson by the ID
        @NamedQuery(name = "FindLessonById",
                query = "SELECT cD FROM Lesson cD WHERE cD.id = :id")
})
public class Lesson {

    public static enum NAMED_QUERIES {GetAllLessons, GetLessonsByCourseId, GetLessonsByCourseAndDay, FindLessonById}

    @Id
    @GeneratedValue
    private Long id;

    private java.sql.Date date;
    private java.sql.Time time;

    @Size(min = 0, max = 50, message = "Location length is maximum 50")
    @NotNull
    private String location;

    @ManyToOne
    private Course course;

    // List of which Person:s attended this Lesson (as students)
    @ManyToMany
    @JoinTable(name = "Attendance",
            joinColumns = @JoinColumn(name = "person_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "lesson_id", referencedColumnName = "id"))
    private List<Person> participants;

    public Lesson() {
    }

    public Lesson(Date date, Time time, String location, Course course) {
        this.date = date;
        this.time = time;
        this.location = location;
        this.course = course;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Person> getParticipants() {
        return participants;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    /**
     * Note a given student as attending this Lesson
     * @param student the student that attended this lesson
     */
    public void addAttendingStudent(Person student) {
        this.participants.add(student);
    }

    /**
     * Note a given student as NOT attending this Lesson
     * @param student the student that did NOT attended this lesson
     */
    public void removeAttendingStudent(Person student) {
        for (int i = 0; i < this.participants.size(); i++) {
            if (student.getId().equals(this.participants.get(i).getId())) {
                this.participants.remove(i);
                break;
            }
        }
    }

    @Override
    public String toString() {
        return "Lesson: " + this.getDate() + " " + this.getTime() +
                ", location=" + this.getLocation() + ", course=" +
                this.getCourse().getName() + " {...}";
    }
}
