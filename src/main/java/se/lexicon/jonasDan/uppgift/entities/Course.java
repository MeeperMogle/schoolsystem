package se.lexicon.jonasDan.uppgift.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-08)
 */
@Entity
@NamedQueries({
        // Returns all Courses
        @NamedQuery(name = "GetAllCourses",
                query = "SELECT c FROM Course c"),
        // Returns all Courses where the user is the teacher
        @NamedQuery(name = "GetCoursesByTeacher",
                query = "SELECT c FROM Course c WHERE lower(c.teacher.name) LIKE lower(:teacher)"),
        // Returns courses containing the search string
        @NamedQuery(
                name = "FindCourseByName",
                query = "SELECT c FROM Course c WHERE lower(c.name) LIKE lower(:courseName)"),
        // Returns courses by the ID
        @NamedQuery(name = "FindCourseById",
                query = "SELECT c FROM Course c WHERE c.id = :id")
})

public class Course {

    private static final String START_DAY_COLUMN_NAME = "start_day";

    public static enum NAMED_QUERIES {GetAllCourses, GetCoursesByTeacher, FindCourseByName, FindCourseById}

    @Id
    @GeneratedValue
    private Long id;

    @Size(min = 3, max = 40, message = "Course name length is 3-40")
    @NotNull
    private String name;

    @Size(min = 4, max = 10, message = "Course code length is 4-10")
    @NotNull
    @Column(unique = true)
    private String code;

    @ManyToOne
    private Level level;

    @Size(min = 2, max = 20, message = "Course language length is 2-20")
    private String language;

    @Min(0)
    @NotNull
    private int maxStudents;

    @Column(name = START_DAY_COLUMN_NAME)
    private Date startDay;

    @ManyToOne
    private Person teacher;

    @ManyToMany(mappedBy = "courses")
    private List<Person> students;

    public Course() {

    }

    public Course(String name, String code, Level level, String language, int maxStudents, Date startDay, Person teacher) {
        this.name = name;
        this.code = code;
        this.level = level;
        this.language = language;
        this.maxStudents = maxStudents;
        this.startDay = startDay;
        this.teacher = teacher;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public Date getStartDay() {
        return startDay;
    }

    public void setStartDay(Date startDay) {
        this.startDay = startDay;
    }

    public Person getTeacher() {
        return teacher;
    }

    public void setTeacher(Person teacher) {
        this.teacher = teacher;
    }

    public List<Person> getStudents() {
        return this.students;
    }

    /**
     * Add the given person as a student to this Course
     * @param student student to add
     */
    public void addStudent(Person student) {
        this.students.add(student);
    }

    /**
     * Remove the given person as a student from this Course
     * @param student student to remove
     */
    public void removeStudent(Person student) {
        for (int i = 0; i < this.students.size(); i++) {
            if (student.getId().equals(this.students.get(i).getId())) {
                this.students.remove(i);
                break;
            }
        }
    }

    @Override
    public String toString() {
        return "Course: " + this.getName() + ", code=" + this.getCode() +
                ", level=" + this.getLevel().toString() + ", language=" +
                this.getLanguage() + ", maxStudents=" + this.getMaxStudents() +
                ", startDay=" + this.getStartDay() + ", teacher=" +
                this.getTeacher().getName() + " {...}";
    }
}
