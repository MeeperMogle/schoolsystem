package se.lexicon.jonasDan.uppgift.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author Jonas Olsson (2016-09-08)
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "GetAllLogins",
                query = "SELECT l FROM Login l")
})
public class Login {

    public enum NAMED_QUERIES {GetAllLogins}

    @Id
    @GeneratedValue
    private Long id;

    @Size(min = 3, max = 10, message = "Username length should be 3-10")
    @NotNull
    @Column(unique = true)
    @Pattern(regexp = "[a-z0-9_]+", flags = Pattern.Flag.CASE_INSENSITIVE,
            message = "Username can only contain letters (A-Z), numbers and underscore (_)")
    private String username;

    @Size(min = 6, message = "Passwords must be minimum 6 characters")
    @NotNull
    @Pattern(regexp = "[a-z0-9!@#&/)(=?-_]+", flags = Pattern.Flag.CASE_INSENSITIVE,
            message = "Password contains invalid characters")
    private String password;

    @ManyToOne
    private Role role;

    public Login() {
    }

    public Login(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Login: " + this.username + ", password=<hidden>, role=" +
                this.getRole().getName();
    }
}
