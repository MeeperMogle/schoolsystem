package se.lexicon.jonasDan.uppgift.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Jonas Olsson (2016-09-08)
 */
@NamedQueries({
        @NamedQuery(name = "GetAllLevels",
                query = "SELECT lev FROM Level lev"),

        @NamedQuery(name = "GetLevelNames",
                query = "Select lev.name from Level lev"),

        @NamedQuery(name = "GetLevelById",
                query = "Select lev from Level lev where lev.id = :id")
})

@Entity
public class Level {

    public static enum NAMED_QUERIES {GetAllLevels, GetLevelById}

    @Id
    @GeneratedValue
    private Long id;

    @Size(min = 5, max = 20, message = "Level name length should be 5-20")
    @NotNull
    @Column(unique = true)
    private String name;

    public Level() {
    }

    public Level(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Level: " + this.getName();
    }
}
