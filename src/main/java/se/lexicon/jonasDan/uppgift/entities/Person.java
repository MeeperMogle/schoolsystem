package se.lexicon.jonasDan.uppgift.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-08)
 */

@Entity
@NamedQueries({
        // Returns all Persons (just for admin)
        @NamedQuery(name = "GetAllPersons",
                query = "SELECT p FROM Person p"),
        // Returns all Persons the teacher is teaching
        @NamedQuery(name = "GetPersonsAndCoursesByTeacherName",
                query = "SELECT p FROM Person p, Course c WHERE c.teacher.name = :teacher"),
        // Returns persons containing the search string
        @NamedQuery(
                name = "FindPersonByName",
                query = "SELECT p FROM Person p WHERE lower(p.name) LIKE lower(:name)"),
        // Returns persons by the ID
        @NamedQuery(name = "FindPersonById",
                query = "SELECT p FROM Person p WHERE p.id = :id"),

        @NamedQuery(name = "findMatchingLoginDetails",
                query = "SELECT l FROM Person p, Login l WHERE p.id = :pId AND l.id = :id"),

        //TODO Find all Persons with login
        @NamedQuery(name = "FindAllPersonWithLogin",
                query = "SELECT s FROM Person s"
        )
})
public class Person {
    private static final String PERS_NR_COLUMN_NAME = "p_nr";

    public static enum NAMED_QUERIES {
        GetAllPersons,
        GetPersonsByTeacherName, FindPersonByName, FindPersonById,
        FindAllPersonWithLogin, findPersonsWithMatchingLoginDetails
    }

    ;

    @Id
    @GeneratedValue
    private Long id;

    @Size(min = 3, max = 50, message = "Name length must be 3-50")
    @NotNull
    private String name;


    @Pattern(regexp = "[0-9]{6,8}[-x][0-9]{4}", flags = Pattern.Flag.CASE_INSENSITIVE,
            message = "Please use format YYYYMMDD-#### or YYMMDD-####")
    @Size(max = 13, message = "No personal number is above 13 characters")
    @NotNull
    @Column(name = PERS_NR_COLUMN_NAME, unique = true)
    private String pNr;


    @Pattern(regexp = "[+]?[0-9]+([ ?\\- ?][0-9])*( ?[0-9])*",
            message = "Please review phone number; only numbers, spaces, - and + allowed")
    @Size(max = 15, message = "15 characters allowed for phone number")
    @NotNull
    private String phone;


    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            message = "Invalid email format", flags = Pattern.Flag.CASE_INSENSITIVE)
    @Size(max = 50, message = "Max character limit of email is 50, how did you get here?!")
    @NotNull
    private String email;

    @Size(max = 30, message = "Max city name length is 30")
    @NotNull
    private String city;

    // One Person can have several logins, such as a Student and Teacher one
    @OneToMany
    private List<Login> accountDetails;

    @ManyToMany
    @JoinTable(name = "Course_Registration",
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id"))
    private List<Course> courses;

    @ManyToMany(mappedBy = "participants")
    private List<Lesson> attendedLessons;

    public Person() {
    }

    public Person(String name, String pNr, String phone, String email, String city) {
        this.name = name;
        this.pNr = pNr;
        this.phone = phone;
        this.email = email;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPNr() {
        return pNr;
    }

    public void setPNr(String pNr) {
        this.pNr = pNr;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Login> getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(List<Login> accountDetails) {
        this.accountDetails = accountDetails;
    }

    public List<Course> getCourses() {
        return courses;
    }

    /**
     * Add given Course to this student's course registrations.
     * @param course course to register the student as partaking in
     */
    public void addCourse(Course course) {
        // Skip the add if the course is already in the list
        for (int i = 0; i < this.courses.size(); i++) {
            if (this.courses.get(i).getId().equals(course.getId())) {
                return;
            }
        }
        this.courses.add(course);
    }

    /**
     * Remove given Course from this student's course registrations.
     * @param course course to de-register the student from
     */
    public void removeCourse(Course course) {
        for (int i = 0; i < this.getCourses().size(); i++) {
            if (this.getCourses().get(i).getId().equals(course.getId())) {
                this.getCourses().remove(i);
                break;
            }
        }
    }

    /**
     * Note this student as attending the given Lesson
     * @param lesson lesson this student attended
     */
    public void addAttendedLesson(Lesson lesson) {
        // Skip the add if the course is already in the list
        for (int i = 0; i < this.attendedLessons.size(); i++) {
            if (this.attendedLessons.get(i).getId().equals(lesson.getId())) {
                return;
            }
        }
        this.attendedLessons.add(lesson);
    }

    /**
     * Remove note of this student's attendance to the given Lesson
     * @param lesson lesson this student did not attend
     */
    public void removeAttendedLesson(Lesson lesson) {
        for (int i = 0; i < this.attendedLessons.size(); i++) {
            if (this.attendedLessons.get(i).getId().equals(lesson.getId())) {
                this.attendedLessons.remove(i);
                break;
            }
        }
    }

    @Override
    public String toString() {
        String loginDetails = "";
        if (this.getAccountDetails() != null) {
            loginDetails = ", #accountDetails=" + this.getAccountDetails().size() +
                    "\n{";
            for (Login login : this.getAccountDetails()) {
                loginDetails += login.toString() + "\n";
            }
            loginDetails += "}";
        }
        return "Person: " + this.getName() + ", pnr=" + this.getPNr() +
                ", phone=" + this.getPhone() + ", email=" + this.getEmail() +
                ", city=" + this.getCity() +
                loginDetails;
    }
}
