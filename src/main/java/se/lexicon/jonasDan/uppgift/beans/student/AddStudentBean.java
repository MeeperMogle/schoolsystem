package se.lexicon.jonasDan.uppgift.beans.student;

import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Login;
import se.lexicon.jonasDan.uppgift.entities.Person;
import se.lexicon.jonasDan.uppgift.entities.Role;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;

/**
 * @author Dan Gunnarsson
 */

@Named
@Stateful
@RequestScoped
public class AddStudentBean {

    private Long id;
    private String name;
    private String pNr;
    private String phone;
    private String email;
    private String city;
    private String username;
    private String password;

    @Inject
    private Persistor storer;

    public String addStudent() {

        Person p = new Person(name, pNr, phone, email, city);

        Role studentRole = storer.getRoleByRoleName("Student");

        Login l = new Login(username, password, studentRole);

        p.setAccountDetails(Arrays.asList(l));

        storer.persistLogin(l);
        storer.persistPerson(p);

        return Navigator.navigate("student/list");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPNr() {
        return pNr;
    }

    public void setPNr(String pNr) {
        this.pNr = pNr;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
