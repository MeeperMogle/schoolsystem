package se.lexicon.jonasDan.uppgift.beans.course;

import se.lexicon.jonasDan.uppgift.beans.LoginBean;
import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Lesson;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

/**
 * @author Dan Gunnarsson on 2016-09-26.
 */

@Named
@Stateful
@SessionScoped
public class EditCourseBean implements Serializable {

    private Course course;
    private Long selectedTeacherId;
    private Long selectedLevelId;

    // Date needs to be gotten as a String and converted manually
    // before getting added to the object.
    private String startDate;

    @Inject
    private Persistor storer;

    @Inject
    private LoginBean credentials;

    public void loadGottenId() {
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get("id"));

            this.course = storer.getCourseById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    public String save() {
        Course course;
        Date d = this.convertStringToDate(startDate);
        this.course.setStartDay(d);
        this.course.setLevel(storer.getLevelById(selectedLevelId));
        this.course.setTeacher(storer.getPersonById(selectedTeacherId));
        course = this.course;

        storer.updateCourse(course);
        selectedLevelId = null;
        selectedTeacherId = null;
        return Navigator.navigate("course/show") + "id=" + course.getId();
    }

    /**
     * @param date yyyy-mm-dd
     * @return sql.Date of given date
     */
    private Date convertStringToDate(String date) {

        Date d = java.sql.Date.valueOf(date);
        return d;
    }

    public String getStartDate() {
        if (startDate == (null)) {
            return this.course.getStartDay().toString();
        }
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Course getCourse() {
        return course;
    }

    public Long getSelectedLevelId() {
        if (selectedLevelId == null) {
            selectedLevelId = this.course.getLevel().getId();
        }
        return selectedLevelId;
    }

    public void setSelectedLevelId(Long selectedLevelId) {
        this.selectedLevelId = selectedLevelId;
    }

    public Long getSelectedTeacherId() {
        if (selectedLevelId == null) {
            selectedLevelId = this.course.getTeacher().getId();
        }
        return selectedTeacherId;
    }

    public void setSelectedTeacherId(Long selectedTeacherId) {
        this.selectedTeacherId = selectedTeacherId;
    }


    /**
     * Delete the Course with the given id from the database.
     * <br>Attempts to recursively delete dependencies.
     * @param idRemove
     * @return
     */
    public String delete(Long idRemove){
        if (canDelete()) {
            List<Lesson> lessons = storer.loadAllLessons();
            for (Lesson l: lessons) {

               if (l.getCourse().getId().equals(idRemove)){
                   storer.lessonRemove(l.getId());
               }
            }

            storer.courseRemove(idRemove);
            return Navigator.navigate("course/list");
        }
        return null;
    }
    public boolean canDelete() {
        return credentials.isAdministratorAccess();
    }
}
