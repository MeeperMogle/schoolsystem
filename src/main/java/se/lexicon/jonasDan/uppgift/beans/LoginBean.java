package se.lexicon.jonasDan.uppgift.beans;

import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Login;
import se.lexicon.jonasDan.uppgift.entities.Person;
import se.lexicon.jonasDan.uppgift.entities.Role;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Handle logging (and staying) in the site as well as access control.
 *
 * @author Dan Gunnarsson
 * @author Jonas Olsson
 */

@Named
@Stateful
@SessionScoped
public class LoginBean implements Serializable {

    // Required credentials
    // Note that password is set to null once login has been performed.
    private String username;
    private String password;

    // Additional session information
    private Role userRole = null;
    private Person userPerson = null;

    @Inject
    private Persistor storer;

    /**
     * Login procedure; compare entered credentials to database.
     *
     * @return location string of where to redirect the user
     */
    public String login() {

        if (this.getUsername() != null && this.getPassword() != null) {

            // Load all logins, for looping
            List<Login> allLogins = storer.loadAllLogins();

            if (allLogins.size() > 0) {

                for (int i = 0; i <= allLogins.size(); i++) {
                    Login currentLogin = allLogins.get(i);

                    // If matching credentials have been found
                    if (currentLogin.getUsername().equals(username) &&
                            currentLogin.getPassword().equals(password)) {

                        // Check whether they match that of an Administrator
                        for (Person p : storer.loadAllAdministrators()) {
                            for (Login l : p.getAccountDetails()) {
                                if (currentLogin.getUsername().equals(l.getUsername()) &&
                                        currentLogin.getPassword().equals(l.getPassword())) {
                                    userPerson = p;
                                }
                            }
                        }
                        // If not an Administrator,
                        // check whether they match that of a Teacher
                        if (userPerson == null) {
                            for (Person p : storer.loadAllTeachers()) {
                                for (Login l : p.getAccountDetails()) {
                                    if (currentLogin.getUsername().equals(l.getUsername()) &&
                                            currentLogin.getPassword().equals(l.getPassword())) {
                                        userPerson = p;
                                    }
                                }
                            }
                        }
                        // If not a Teacher,
                        // check whether they match that of a Student
                        if (userPerson == null) {
                            for (Person p : storer.loadAllStudents()) {
                                for (Login l : p.getAccountDetails()) {
                                    if (currentLogin.getUsername().equals(l.getUsername()) &&
                                            currentLogin.getPassword().equals(l.getPassword())) {
                                        userPerson = p;
                                    }
                                }
                            }
                        }

                        // Set the session Role object
                        userRole = currentLogin.getRole();

                        // Have found correct login information, can stop
                        break;
                    }
                }
            }
        }
        // Empty password from session
        this.setPassword(null);

        // Generate the location link for redirection
        return getProfileLink();
    }

    /**
     * Generate location String for redirection after login.
     * <br>Based on login credentials role, such as Student, Teacher...
     *
     * @return location string using Navigator, &id=# appended
     */
    public String getProfileLink() {
        String returnString = "login";
        try {
            switch (userRole.getName().toLowerCase()) {
                case "student":
                    returnString = "student/show";
                    break;

                case "teacher":
                    returnString = "teacher/show";
                    break;

                case "administrator":
                    returnString = "administrator/show";
                    break;
            }
        } catch (NullPointerException NPE) {
            System.out.println("Error was:" + NPE.getMessage());
            return Navigator.navigate("login");
        }
        return Navigator.navigate(returnString) + "&id=" + userPerson.getId();
    }

    /**
     * Reset session by setting variables to null
     *
     * @return navigation string to the site index
     */
    public String logOut() {
        this.username = null;
        this.password = null;
        this.userRole = null;
        this.userPerson = null;
        return Navigator.navigate("index");
    }

    /**
     * Check whether the user is logged in, based on existing session variables.
     * <br>Affecting variables is username && userRole && userPerson
     *
     * @return true if logged in, else false
     */
    public boolean isLoggedIn() {
        return this.username != null &&
                this.userRole != null &&
                this.userPerson != null;
    }

    /**
     * Check whether user has the access level of a Teacher.
     *
     * @return true if credentials include Teacher, false if not
     */
    public boolean isTeacherAccess() {
        return isRoleAccess("Teacher");
    }

    /**
     * Check whether user has the access level of a Student.
     *
     * @return true if credentials include Student, false if not
     */
    public boolean isStudentAccess() {
        return isRoleAccess("Student");
    }

    /**
     * Check whether user has the access level of a Administrator.
     *
     * @return true if credentials include Administrator, false if not
     */
    public boolean isAdministratorAccess() {
        return isRoleAccess("Administrator");
    }

    /**
     * General method; check whether user has the given access level.
     *
     * @param roleName textual name of the access level
     * @return true if credentials include given access level, false if not
     */
    private boolean isRoleAccess(String roleName) {
        return this.isLoggedIn() &&
                this.getUserRole().getName().equalsIgnoreCase(roleName);
    }

    /**
     * Checks whether the current user has access to view the Attendance
     * statistics about the Person with the given id.
     * @param id Person whose attendance info is getting accessed
     * @return true: administrator access, or it's themselves. false: otherwise
     */
    public boolean hasPersonAttendanceAccess(Long id) {
        try {
            Person supposedPerson = storer.getPersonById(id);
            if (supposedPerson != null) {
                return this.isAdministratorAccess() ||
                        supposedPerson.getId() == this.userPerson.getId();
            } else {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Checks whether the current user has access to view the Attendance
     * statistics about the Course with the given id.
     * @param id Course which attendance info is getting accessed
     * @return true: administrator access, or it's their own course. false: otherwise
     */

    public boolean hasCourseAttendanceAccess(Long id) {
        try {
            Course supposedCourse = storer.getCourseById(id);
            if (supposedCourse != null) {
                return this.isAdministratorAccess() ||
                        supposedCourse.getTeacher().getId() ==
                                this.userPerson.getId();
            } else {
                return false;
            }
        } catch (NumberFormatException|NullPointerException e) {
            return false;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getUserRole() {
        return userRole;
    }

    public Person getUserPerson() {
        return userPerson;
    }

    /**
     * Checks whether the current user has access to handle course registrations
     * for the specified user.
     * @param person person who's course registrations are being manipulated
     * @return true: administrator access, or it's themselves. false: otherwise
     */
    public boolean hasAccessToManageCourseRegistrationsFor(Long person) {
        return this.isAdministratorAccess() ||
                (this.getUserPerson() != null &&
                        this.getUserPerson().getId() == person);
    }
}
