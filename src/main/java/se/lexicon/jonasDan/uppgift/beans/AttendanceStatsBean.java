package se.lexicon.jonasDan.uppgift.beans;

import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Lesson;
import se.lexicon.jonasDan.uppgift.entities.Person;
import se.lexicon.jonasDan.uppgift.entities.Role;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Jonas Olsson (2016-09-26)
 * Handle the fetching, filtering and display of attendance-statistics.
 */

@RequestScoped
@Stateful
@Named
public class AttendanceStatsBean {

    // When focus-filtering is applied, loop through and remove
    // all Lesson without such an applicable connection

    private List<Lesson> lessonsCoveredBySearch;

    @Inject
    private Persistor loader;

    @PostConstruct
    /**
     * Loads all lessons. Then filters out unwanted ones based on GET parameters.
     * <br>student=# filters all that do not apply to the Person (as a student)
     * <br>course=# filters all that do not apply to the Course
     */
    public void loadAttendanceBasedOnGetParams() {
        this.lessonsCoveredBySearch = loader.loadAllLessons();

        Map<String, String> requestParams = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        // Look for student=#, then focus in on that Student
        Long studentId = null;
        try {
            studentId = Long.parseLong(requestParams.get("student"));
            focusAttendanceOnStudent(studentId);
        } catch (NumberFormatException nfE) {
            // Invalid student ID
        }

        // Look for course=#, then focus in on that Course
        Long courseId = null;
        try {
            courseId = Long.parseLong(requestParams.get("course"));
            focusAttendanceOnCourse(courseId);
        } catch (NumberFormatException nfE) {
            // Invalid student ID
        }

        // TODO: teacher=# to filter out all that's not taught by that teacher Person

        // TODO: date=yyyy-mm-dd to filter out all not on that day

    }

    /**
     * Filter out all Lesson:s from the List that have no connection
     * to the Course with the given id.
     * @param courseId id of the relevant Course
     */
    private void focusAttendanceOnCourse(Long courseId) {
        Course course = loader.getCourseById(courseId);

        if (course != null) {
            // Remove Lesson's not in this Course

            List<Lesson> filterLessons = new ArrayList<>();
            for (Lesson lesson : this.lessonsCoveredBySearch) {
                if (lesson.getCourse().getId().equals(course.getId())) {
                    filterLessons.add(lesson);
                }
            }

            this.lessonsCoveredBySearch = filterLessons;
        }
    }

    /**
     * Filter out all Lesson:s from the List that have no connection
     * to the Person (<i>as a Student</i>) with the given id.
     * @param id id of the relevant Course
     */
    public void focusAttendanceOnStudent(Long id) {
        try {
            Person student = loader.getPersonById(id);

            if (student != null) {
                // Only actually keep searching if they have a Student login
                List<Role> studentRoles = new ArrayList<>();
                student.getAccountDetails().
                        forEach(l -> studentRoles.add(l.getRole()));

                if (Role.containsRoleWithName(studentRoles, "Student")) {

                    // Collect unique Courses, the ones that have the Student
                    // are then moved to applicableCourses
                    Set<Course> uniqueCourses = new HashSet<>();
                    Set<Course> applicableCourses = new HashSet<>();

                    // Loop through Lesson, put their Courses in unique set...
                    for (Lesson lesson : this.lessonsCoveredBySearch) {
                        Course currentCourse = lesson.getCourse();

                        // ... if it was just added, see if this student is registered...
                        if (uniqueCourses.add(currentCourse)) {
                            // Loop through students registered to see if they are there
                            for (Person registeredStudent : currentCourse.getStudents()) {
                                if (registeredStudent.getId().equals(student.getId())) {
                                    applicableCourses.add(currentCourse);
                                    break;
                                }
                            }
                        }
                        // ... if it was not, it has already been searched through for them.
                    }

                    // Remove all Lesson that are not in the list of applicable courses
                    List<Lesson> filterLessons = new ArrayList<>();
                    for (Lesson lesson : this.lessonsCoveredBySearch) {
                        for (Course course : applicableCourses) {
                            if (lesson.getCourse().getId().equals(course.getId())) {
                                filterLessons.add(lesson);
                            }
                        }
                    }
                    lessonsCoveredBySearch = filterLessons;
                }
            }
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("No student-id found in request");
        }
    }

    /**
     * Check whether a Person with the given id has attended the given Lesson
     * (as a student).
     * @param lesson lesson to check through attendees of
     * @param id id of the Person that may or may not have attended
     * @return true: Person is in attended-list. false: person is not in attended-list
     */
    public boolean personAttendedWhoHasId(Lesson lesson, Long id) {
        for (Person attendee :
                lesson.getParticipants()) {
            if (attendee.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check number of Lesson:s not filtered out (the ones left to be shown).
     * @return number of Lesson:s that are still not filtered out
     */
    public int totalApplicableLessons() {
        return this.lessonsCoveredBySearch.size();
    }

    /**
     * Count number of Lesson:s the Person with the given id has attended.
     * @param id Person id of student to check attendance number for
     * @return number of Lesson which has this Person-id in attended-list
     */
    public int totalAttendedLessons(Long id) {
        int total = 0;

        // Count number of classes actually attended by the person
        // of all that are in the list.
        for (Lesson lesson : this.lessonsCoveredBySearch) {
            if (this.personAttendedWhoHasId(lesson, id)) {
                total++;
            }
        }

        return total;
    }

    /**
     * Count number of students that attended the given Lesson.
     * @param lesson lesson to count students of
     * @return number of students that are in the attended-list of the Lesson
     */
    public int totalAttendees(Lesson lesson) {
        return lesson.getParticipants().size();
    }

    /**
     * Check total number of Person:s registered to the given Course (as students).
     * @param course course to check the student count of
     * @return number of students registered on the given Course
     */
    public int totalCourseStudents(Course course) {
        return course.getStudents().size();
    }

    public float percentageDivision(int attended, int total) {
        return (float) attended / (float) total;
    }

    /**
     * Calculate the average student attendance of the Lesson:s in the list.
     * @return average (percentage) of student attendance in the Lessons left
     */
    public float getAverageAttendancePercentage() {
        float total = 0;

        for (Lesson lesson : lessonsCoveredBySearch) {
            total += (float) lesson.getParticipants().size() /
                    (float) lesson.getCourse().getStudents().size();
        }

        float average = total / lessonsCoveredBySearch.size();

        return average;
    }

    public List<Lesson> getLessonsCoveredBySearch() {
        return lessonsCoveredBySearch;
    }
}
