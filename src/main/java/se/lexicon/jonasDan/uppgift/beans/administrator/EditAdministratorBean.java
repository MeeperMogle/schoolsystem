package se.lexicon.jonasDan.uppgift.beans.administrator;

import se.lexicon.jonasDan.uppgift.beans.LoginBean;
import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Date;

/**
 * @author Dan Gunnarsson on 2016-09-26.
 */

@Named
@Stateful
@SessionScoped
public class EditAdministratorBean implements Serializable{

    Person admin;

    @Inject
    Persistor storer;

    @Inject
    private LoginBean credentials;

    public void loadGottenId(){
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get("id"));

            this.admin = storer.getPersonById(id);
        } catch(NumberFormatException|EJBException |NullPointerException e){
            System.out.println("the error: " + e);
        }
    }

    public String save(){
        Person admin;
        System.out.println(numberOfAdminsBiggerThanOne());
        admin = this.admin;

        storer.updatePerson(admin);
        return Navigator.navigate("administrator/show") + "id=" + admin.getId();
    }

    public Person getAdmin() {
        return admin;
    }

    public boolean numberOfAdminsBiggerThanOne(){

        return(storer.loadAllAdministrators().size() > 1);
    }

    /**
     * Delete the Person with the given id from the database.
     * <br>Does nothing if there is only 1 administrator or less left.
     * @param idRemove
     * @return
     */

    public String delete(Long idRemove){
        if (numberOfAdminsBiggerThanOne()) {

            if (canDelete()) {
                storer.adminRemove(idRemove);
                return Navigator.navigate("administrator/list");
            }
        }
        return null;
    }

    public boolean canDelete(){
        return numberOfAdminsBiggerThanOne() && credentials.isAdministratorAccess();
    }
}
