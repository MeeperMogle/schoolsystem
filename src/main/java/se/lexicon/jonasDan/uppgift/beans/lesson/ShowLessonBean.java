package se.lexicon.jonasDan.uppgift.beans.lesson;

import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Lesson;
import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author Dan Gunnarsson
 */

@Named
@Stateful
@SessionScoped
public class ShowLessonBean implements Serializable {

    private Long id;

    private Lesson lesson;

    @Inject
    private Persistor storer;

    /**
     * Fetch the ID to edit from the GET parameters or,
     * if that has been lost, let it remain as it was since last time.
     */
    public void loadGottenId() {
        // ID from the JSF page context
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get("id"));

            this.lesson = storer.getLessonById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    /**
     * Check whether the given Person attended this Lesson (as a student)
     * @param studentId id of the student in question
     * @return true: they are in the attended-list. false: they are not
     */
    public boolean studentAttended(Long studentId) {
        for (Person person : this.getLesson().getParticipants()) {
            if (person.getId().equals(studentId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add the Person with this id to the attended-list.
     * @param studentId person to add
     */
    public void addStudentToLesson(Long studentId) {
        Person student = storer.getPersonById(studentId);
        student.addAttendedLesson(this.getLesson());
        lesson.addAttendingStudent(student);
        storer.updatePerson(student);
        storer.updateLesson(this.getLesson());
    }

    /**
     * Remove the Person with this id from the attended-list.
     * @param studentId person to remove
     */
    public void removeStudentFromLesson(Long studentId) {
        Person student = storer.getPersonById(studentId);
        student.removeAttendedLesson(this.getLesson());
        lesson.removeAttendingStudent(student);
        storer.updatePerson(student);
        storer.updateLesson(this.getLesson());
    }

    public Lesson getLesson() {
        return lesson;
    }
}
