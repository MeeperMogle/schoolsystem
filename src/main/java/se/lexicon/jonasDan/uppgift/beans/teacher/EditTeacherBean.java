package se.lexicon.jonasDan.uppgift.beans.teacher;

import se.lexicon.jonasDan.uppgift.beans.LoginBean;
import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Login;
import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author Dan Gunnarsson on 2016-09-26.
 */

@Named
@Stateful
@SessionScoped
public class EditTeacherBean implements Serializable {

    private Person teacher;

    @Inject
    private Persistor storer;

    @Inject
    private LoginBean credentials;

    /**
     * Fetch the ID to edit from the GET parameters or,
     * if that has been lost, let it remain as it was since last time.
     */
    public void loadGottenId() {
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get("id"));

            this.teacher = storer.getPersonById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    public String save() {
        Person teacher;

        teacher = this.teacher;

        storer.updatePerson(teacher);
        return Navigator.navigate("teacher/show") + "id=" + teacher.getId();
    }

    public Person getTeacher() {
        return teacher;
    }

    /**
     * Delete the Person with the given id from the database.
     * <br>Attempts to recursively delete dependencies.
     * @param idRemove
     * @return
     */
    public String delete(Long idRemove){
        if (canDelete()) {

            Person teacher = storer.getPersonById(idRemove);
            teacher.getCourses().forEach(c -> storer.courseRemove(c.getId()));
            for (Login login : teacher.getAccountDetails() ) {
                if (login.getRole().getName().equalsIgnoreCase("Teacher")){
                    storer.loginRemove(login.getId());
                }
            }

            if (teacher.getAccountDetails().size() == 0){
                storer.teacherRemove(teacher.getId());
            }

            return Navigator.navigate("teacher/list");
        }
        return null;
    }

    public boolean canDelete() {
        return credentials.isAdministratorAccess();
    }
}
