package se.lexicon.jonasDan.uppgift.beans.student;

import se.lexicon.jonasDan.uppgift.beans.LoginBean;
import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Login;
import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * @author Dan Gunnarsson on 2016-09-26.
 */

@Named
@Stateful
@SessionScoped
public class EditStudentBean implements Serializable {

    private Person student;

    @Inject
    private Persistor storer;

    @Inject
    private LoginBean credentials;

    /**
     * Fetch the ID to edit from the GET parameters or,
     * if that has been lost, let it remain as it was since last time.
     */
    public void loadGottenId() {
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get("id"));

            this.student = storer.getPersonById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    public String save() {
        Person student;

        student = this.student;

        storer.updatePerson(student);
        return Navigator.navigate("student/show") + "id=" + student.getId();
    }

    public Person getStudent() {
        return student;
    }


    /**
     * Delete the Person with the given id from the database.
     * <br>Attempts to recursively delete dependencies.
     * @param idRemove
     * @return
     */
    public String delete(Long idRemove) {
        if (canDelete()) {

            Person student = storer.getPersonById(idRemove);

            for (Login login : student.getAccountDetails()) {
                if (login.getRole().getName().equalsIgnoreCase("Student")) {
                    storer.loginRemove(login.getId());
                }
            }

            if (student.getAccountDetails().size() == 0) {
                storer.studentRemove(student.getId());
            }

            List<Course> cList = student.getCourses();

            for (Course c : cList) {
                storer.courseRemove(c.getId());
            }

            storer.studentRemove(idRemove);
            return Navigator.navigate("student/list");
        }
        return null;
    }

    public boolean canDelete() {
        return credentials.isAdministratorAccess();
    }
}
