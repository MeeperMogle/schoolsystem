package se.lexicon.jonasDan.uppgift.beans.lesson;

import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Lesson;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * @author Dan Gunnarsson
 */

@Named
@Stateful
@SessionScoped
public class AddLessonBean implements Serializable {

    private Long id;

    // Date and Time need to be gotten as a String,
    // then converted and added.
    private String date;
    private String time;

    private String location;
    private Course course;

    @Inject
    private Persistor storer;

    public String addLesson() {
        System.out.println("added Lesson");

        Date d = convertStringToDate(date);
        Time t = convertStringToTime(time);

        Lesson l = new Lesson(d, t, location, course);

        storer.persistLesson(l);

        return Navigator.navigate("course/show") + "id=" + this.course.getId();
    }

    public void loadGottenId() {
        // ID from the JSF page context
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get("course"));

            this.course = storer.getCourseById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    /**
     * @param date yyyy-mm-dd
     * @return sql.Date of given date
     */
    private Date convertStringToDate(String date) {
        Date d = Date.valueOf(date);
        return d;
    }

    /**
     * @param time yyyy-mm-dd
     * @return sql.Date of given date
     */
    private Time convertStringToTime(String time) {
        Time t = Time.valueOf(time);
        return t;
    }

    public Long getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Course getCourse() {
        return course;
    }
}
