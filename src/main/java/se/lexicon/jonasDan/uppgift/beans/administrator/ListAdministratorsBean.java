package se.lexicon.jonasDan.uppgift.beans.administrator;

import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-12)
 */
@RequestScoped
@Stateful
@Named
public class ListAdministratorsBean {
    private String searchName = "";

    private List<Person> searchResults = null;

    @Inject
    private Persistor loader;

    @PostConstruct
    private void listAllStudents() {
        this.searchResults = loader.loadAllAdministrators();
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;

        // Setting search name? Update list.
        this.searchResults = loader.loadMatchingAdministratorNames(this.getSearchName());
    }

    public int getSearchResultsAmount() {
        if (searchResults != null) {
            return searchResults.size();
        } else {
            return 0;
        }
    }

    public List<Person> getSearchResults() {
        return searchResults;
    }
}
