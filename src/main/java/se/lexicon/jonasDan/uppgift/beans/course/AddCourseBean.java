package se.lexicon.jonasDan.uppgift.beans.course;

import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Level;
import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Date;

/**
 * @author Dan Gunnarsson
 */

@Named
@Stateful
@RequestScoped
public class AddCourseBean {

    private Long id;
    private String name;
    private String code;
    private Long levelId;
    private String language;
    private int maxStudents;

    // Date needs to be gotten as a String,
    // then converted and added.
    private String startDay;

    private Long teacherId;

    @Inject
    private Persistor storer;

    public String addCourse() {
        System.out.println("added Course");

        Level l = storer.getLevelById(getLevelId());
        Date d = convertStringToDate(startDay);
        Person t = storer.getPersonById(getTeacherId());

        Course c = new Course(getName(), getCode(), l, getLanguage(), getMaxStudents(), d, t);

        storer.persistCourse(c);

        System.out.println(l + "\n" + d + "\n" + t);

        return Navigator.navigate("course/list");
    }

    /**
     * @param date yyyy-mm-dd
     * @return sql.Date of given date
     */
    private Date convertStringToDate(String date) {

        Date d = java.sql.Date.valueOf(date);
        return d;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }
}
