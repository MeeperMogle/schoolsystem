package se.lexicon.jonasDan.uppgift.beans.lesson;

import se.lexicon.jonasDan.uppgift.beans.LoginBean;
import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Lesson;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * @author Dan Gunnarsson on 2016-09-27.
 */

@Named
@Stateful
@SessionScoped
public class EditLessonBean implements Serializable {

    private Lesson lesson;

    // Date and Time need to be gotten as String,
    // and converted before being added.
    private String date;
    private String time;

    @Inject
    private Persistor storer;

    @Inject
    private LoginBean credentials;

    /**
     * Fetch the ID to edit from the GET parameters or,
     * if that has been lost, let it remain as it was since last time.
     */
    public void loadGottenId() {
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get("id"));

            this.lesson = storer.getLessonById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    public String save() {
        Date d = this.convertStringToDate(date);
        Time t = this.convertStringToTime(time);
        this.lesson.setDate(d);
        this.lesson.setTime(t);

        storer.updateLesson(this.lesson);

        return Navigator.navigate("lesson/show") + "id=" + this.lesson.getId();
    }

    private Date convertStringToDate(String date) {

        Date d = java.sql.Date.valueOf(date);
        return d;
    }

    private Time convertStringToTime(String time) {
        Time t = Time.valueOf(time);
        return t;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public String getDate() {

        if (date == (null)) {
            return this.lesson.getDate().toString();
        }
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        if (time == (null)) {
            return this.lesson.getTime().toString();
        }
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    /**
     * Delete the Lesson with the given id from the database.
     * <br>Attempts to recursively delete dependencies.
     * @param idRemove
     * @return
     */
    public String delete(Long idRemove){
        if (canDelete()) {
            storer.lessonRemove(idRemove);
            return Navigator.navigate("course/show") + "id=" + this.lesson.getCourse().getId();
        }
        return null;
    }

    public boolean canDelete(){
        return credentials.isTeacherAccess() &&
                credentials.getUserPerson().getId().equals(
                        this.getLesson().getCourse().getTeacher().getId()
                );
    }
}
