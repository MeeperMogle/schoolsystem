package se.lexicon.jonasDan.uppgift.beans;

import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Lesson;
import se.lexicon.jonasDan.uppgift.entities.Level;
import se.lexicon.jonasDan.uppgift.entities.Login;
import se.lexicon.jonasDan.uppgift.entities.Person;
import se.lexicon.jonasDan.uppgift.entities.Role;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-09)
 * Class for accessing the underlying database.
 */
@Stateless
public class Persistor {

    @PersistenceContext(unitName = "jonasDan-Persistence-Unit")
    private EntityManager em;

    //<editor-fold desc="persist-methods">

    /**
     * Store given object in the corresponding database table
     * @param role
     */
    public void persistRole(Role role) {
        System.out.println("Persisting role: " + role.toString());
        em.persist(role);
    }

    /**
     * Store given object in the corresponding database table
     * @param login
     */
    public void persistLogin(Login login) {
        System.out.println("Persisting login: " + login.toString());
        em.persist(login);
    }

    /**
     * Store given object in the corresponding database table
     * @param person
     */
    public void persistPerson(Person person) {
        System.out.println("Persisting person: " + person.toString());
        em.persist(person);
    }

    /**
     * Store given object in the corresponding database table
     * @param level
     */
    public void persistLevel(Level level) {
        System.out.println("Persisting level: " + level.toString());
        em.persist(level);
    }

    /**
     * Store given object in the corresponding database table
     * @param course
     */
    public void persistCourse(Course course) {
        System.out.println("Persisting course: " + course.toString());
        em.persist(course);
    }

    /**
     * Store given object in the corresponding database table
     * @param lesson
     */
    public void persistLesson(Lesson lesson) {
        System.out.println("Persisting lesson: " + lesson.toString());
        em.persist(lesson);
    }
    //</editor-fold>

    //<editor-fold desc="Load all-methods">

    /**
     * Loads all of these objects from the <i></i>database</i>
     * @return
     */
    public List<Course> loadAllCourses() {
        TypedQuery<Course> allCoursesQuery = em.createNamedQuery(
                Course.NAMED_QUERIES.GetAllCourses.name(),
                Course.class
        );

        return allCoursesQuery.getResultList();
    }

    /**
     * Loads all of these objects from the <i></i>database</i>
     * @return
     */
    public List<Level> loadAllLevels() {
        TypedQuery<Level> allLevelsQuery = em.createNamedQuery(
                Level.NAMED_QUERIES.GetAllLevels.name(),
                Level.class
        );

        return allLevelsQuery.getResultList();
    }

    /**
     * Loads all of these objects from the <i></i>database</i>
     * @return
     */
    public List<Login> loadAllLogins() {
        TypedQuery<Login> allLoginsQuery = em.createNamedQuery(
                Login.NAMED_QUERIES.GetAllLogins.name(),
                Login.class
        );

        return allLoginsQuery.getResultList();
    }

    /**
     * Loads all of these objects from the <i></i>database</i>
     * @return
     */
    public List<Lesson> loadAllLessons() {
        TypedQuery<Lesson> allLessonsQuery = em.createNamedQuery(
                Lesson.NAMED_QUERIES.GetAllLessons.name(),
                Lesson.class
        );

        return allLessonsQuery.getResultList();
    }

    /**
     * General method for loading all Person-objects
     * which have the given Role-name in their list of logins.
     *
     * @param roleName
     * @return
     */
    private List<Person> loadAllPersonWithRole(String roleName) {
        TypedQuery<Person> allStudentsQuery = em.createNamedQuery(
                Person.NAMED_QUERIES.FindAllPersonWithLogin.name(),
                Person.class
        );

        return filterOutPersonWithoutRoleName(
                allStudentsQuery.getResultList(), roleName);
    }

    /**
     * Load all Person-objects from the database which have the Student-role
     * @return
     */
    public List<Person> loadAllStudents() {
        return loadAllPersonWithRole("Student");
    }

    /**
     * Load all Person-objects from the database which have the Administrator-role
     * @return
     */
    public List<Person> loadAllAdministrators() {
        return loadAllPersonWithRole("Administrator");
    }

    /**
     * Load all Person-objects from the database which have the Teacher-role
     * @return
     */
    public List<Person> loadAllTeachers() {
        return loadAllPersonWithRole("Teacher");
    }
    //</editor-fold>


    //<editor-fold desc="get by id-methods">

    /**
     * Fetch the object, from the database, which has unique ID given
     * @param id
     * @return
     */
    public Level getLevelById(Long id) {
        TypedQuery<Level> levelQuery = em.createNamedQuery(
                Level.NAMED_QUERIES.GetLevelById.name(),
                Level.class
        );

        levelQuery = levelQuery.setParameter("id", id);

        return levelQuery.getSingleResult();
    }

    /**
     * Fetch the object, from the database, which has unique ID given
     * @param id
     * @return
     */
    public Person getPersonById(Long id) {
        TypedQuery<Person> matchingTeacherIdQuery = em.createNamedQuery(
                Person.NAMED_QUERIES.FindPersonById.name(),
                Person.class
        );

        matchingTeacherIdQuery = matchingTeacherIdQuery.setParameter("id", id);

        Person found;
        try {
            found = matchingTeacherIdQuery.getSingleResult();
        } catch (NoResultException e) {
            found = null;
        }

        return found;
    }

    /**
     * Fetch the object, from the database, which has unique ID given
     * @param id
     * @return
     */
    public Course getCourseById(Long id) {
        TypedQuery<Course> matchingCourseIdQuery = em.createNamedQuery(
                Course.NAMED_QUERIES.FindCourseById.name(),
                Course.class
        );

        matchingCourseIdQuery = matchingCourseIdQuery.setParameter("id", id);

        Course found;
        try {
            found = matchingCourseIdQuery.getSingleResult();
        } catch (NoResultException e) {
            found = null;
        }

        return found;
    }

    /**
     * Fetch the object, from the database, which has unique ID given
     * @param id
     * @return
     */
    public Lesson getLessonById(Long id) {
        TypedQuery<Lesson> matchingLessonIdQuery = em.createNamedQuery(
                Lesson.NAMED_QUERIES.FindLessonById.name(),
                Lesson.class
        );

        matchingLessonIdQuery = matchingLessonIdQuery.setParameter("id", id);

        Lesson found;
        try {
            found = matchingLessonIdQuery.getSingleResult();
        } catch (NoResultException e) {
            found = null;
        }

        return found;
    }
    //</editor-fold>

    //<editor-fold desc="get by approximate name-methods">

    /**
     * Load all objects, from the database, whose String name corresponds
     * to the given value.
     * @param name
     * @return
     */
    public List<Course> loadMatchingCourseNames(String name) {
        TypedQuery<Course> matchingCourseNamesQuery = em.createNamedQuery(
                Course.NAMED_QUERIES.FindCourseByName.name(),
                Course.class
        );

        matchingCourseNamesQuery = matchingCourseNamesQuery.
                setParameter("courseName", "%" + name + "%");

        return matchingCourseNamesQuery.getResultList();
    }

    /**
     * Load all objects, from the database, whose String name corresponds
     * to the given value.
     * @param roleName
     * @return
     */
    public Role getRoleByRoleName(String roleName) {
        TypedQuery<Role> matchingRoleIdQuery = em.createNamedQuery(
                Role.NAMED_QUERIES.FindRoleByRoleName.name(),
                Role.class
        );

        matchingRoleIdQuery = matchingRoleIdQuery.setParameter("name", roleName);

        Role found;
        try {
            found = matchingRoleIdQuery.getSingleResult();
        } catch (NoResultException e) {
            found = null;
        }

        return found;
    }

    /**
     * General method for fetching all Person-objects
     * that match a certain approximate name
     * and have a particular Role.
     *
     * @param searchName name to search for
     * @param roleName   role to limit the scope
     * @return list of the results
     */
    private List<Person> loadMatchingPersonRoleNames(String searchName,
                                                     String roleName) {
        TypedQuery<Person> matchingPersonNamesQuery = em.createNamedQuery(
                Person.NAMED_QUERIES.FindPersonByName.name(),
                Person.class
        );

        matchingPersonNamesQuery = matchingPersonNamesQuery.
                setParameter("name", "%" + searchName + "%");

        return filterOutPersonWithoutRoleName(
                matchingPersonNamesQuery.getResultList(),
                roleName);
    }

    /**
     * Load all Person-objects, from the database, whose String name corresponds
     * to the given value and who are also Student:s.
     * @param searchName
     * @return
     */
    public List<Person> loadMatchingStudentNames(String searchName) {
        return loadMatchingPersonRoleNames(searchName, "Student");
    }

    /**
     * Load all Person-objects, from the database, whose String name corresponds
     * to the given value and who are also Administrator:s.
     * @param searchName
     * @return
     */
    public List<Person> loadMatchingAdministratorNames(String searchName) {
        return loadMatchingPersonRoleNames(searchName, "Administrator");
    }

    /**
     * Load all Person-objects, from the database, whose String name corresponds
     * to the given value and who are also Teacher:s.
     * @param searchName
     * @return
     */
    public List<Person> loadMatchingTeacherNames(String searchName) {
        return loadMatchingPersonRoleNames(searchName, "Teacher");
    }
    //</editor-fold>


    /**
     * Filter out all Person-objects in the given list that do not
     * have the given Role.
     *
     * @param all      list of Person
     * @param roleName textual name of in-scope Role
     * @return new list containing only matching Person
     */
    private List<Person> filterOutPersonWithoutRoleName(List<Person> all,
                                                        String roleName) {
        List<Person> onlyRightPersons = new ArrayList<Person>();

        // For every Person in the list
        for (Person p : all) {
            // Loop through all their Login credentials
            for (Login l : p.getAccountDetails()) {
                // If it matches the given Role, include them
                if (l.getRole().getName().equalsIgnoreCase(roleName)) {
                    onlyRightPersons.add(p);
                    break;
                }
            }
        }

        return onlyRightPersons;
    }


    /**
     * Make absolutely sure the Person object is synched between the application
     * and database.
     * @param student person to synch
     */
    public void updatePerson(Person student) {
        em.merge(student);
    }

    /**
     * Make absolutely sure the Course object is synched between the application
     * and database.
     * @param course person to synch
     */
    public void updateCourse(Course course) {
        em.merge(course);
    }

    /**
     * Make absolutely sure the Lesson object is synched between the application
     * and database.
     * @param lesson person to synch
     */
    public void updateLesson(Lesson lesson) {
        em.merge(lesson);
    }


    /**
     * Remove object with given ID from database.
     * @param id
     */
    public void lessonRemove( Long id) {
        Lesson lesson = em.find(Lesson.class, id);
        em.remove(lesson);
    }

    /**
     * Remove object with given ID from database.
     * @param id
     */
    public void courseRemove(Long id){
        Course course = em.find(Course.class, id);
        loadLessonsForCourseId(course.getId()).forEach(l -> lessonRemove(l.getId()));
        em.remove(course);
    }

    /**
     * Remove object with given ID from database.
     * @param id
     */
    public void studentRemove(Long id){
        Person student = em.find(Person.class, id);
        em.remove(student);
    }

    /**
     * Remove object with given ID from database.
     * @param id
     */
    public void teacherRemove(Long id){
        Person teacher = em.find(Person.class, id);
        em.remove(teacher);
    }

    /**
     * Remove object with given ID from database.
     * @param id
     */
    public void adminRemove(Long id){
        Person admin = em.find(Person.class, id);
        em.remove(admin);
    }

    /**
     * Remove object with given ID from database.
     * @param id
     */
    public void loginRemove(Long id){
        Login login = em.find(Login.class, id);
        em.remove(login);
    }

    /**
     * Load all Lesson:s which are connected to the Course with the given ID.
     * @param courseId id of the course which lessons are wanted
     * @return List&lt;Lesson> related to the given Course
     */
    public List<Lesson> loadLessonsForCourseId(Long courseId) {
        TypedQuery<Lesson> matchingLessonsQuery = em.createNamedQuery(
                Lesson.NAMED_QUERIES.GetLessonsByCourseId.name(),
                Lesson.class
        );

        matchingLessonsQuery = matchingLessonsQuery.setParameter("id", courseId);

        return matchingLessonsQuery.getResultList();
    }


}