package se.lexicon.jonasDan.uppgift.beans.level;

import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Level;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dan Gunnarsson
 */

@RequestScoped
@Stateful
@Named
public class ListLevelsBean {

    private List<String> levels;
    private List<Level> searchResults = null;

    @Inject
    private Persistor loader;

    @PostConstruct
    private void listAllLevels() {
        levels = new ArrayList<>();

        this.searchResults = loader.loadAllLevels();

        this.searchResults.forEach(l -> levels.add(l.getName()));
    }

    public int getSearchResultsAmount() {
        if (searchResults != null) {
            return searchResults.size();
        } else {
            return 0;
        }
    }

    public List<Level> getSearchResults() {
        return searchResults;
    }

    public List<String> getLevels() {
        return levels;
    }

    public void setLevels(List<String> levels) {
        this.levels = levels;
    }
}
