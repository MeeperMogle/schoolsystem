package se.lexicon.jonasDan.uppgift.beans.course;

import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Course;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-12)
 */
@RequestScoped
@Stateful
@Named
public class ListCoursesBean {
    private String searchName = "";

    private List<Course> searchResults = null;

    @Inject
    private Persistor loader;

    @PostConstruct
    private void listAllCourses() {
        this.searchResults = loader.loadAllCourses();
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;

        // Setting search name? Update list.
        this.searchResults = loader.loadMatchingCourseNames(this.getSearchName());
    }

    public int getSearchResultsAmount() {
        if (searchResults != null) {
            return searchResults.size();
        } else {
            return 0;
        }
    }

    public List<Course> getSearchResults() {
        return searchResults;
    }

    public List<Course> getCoursesWithTeacherId(Long id) {
        List<Course> validCourses = new ArrayList<>();

        for (Course course :
                this.searchResults) {
            if (course.getTeacher().getId().equals(id)) {
                validCourses.add(course);
            }
        }

        return validCourses;
    }


}
