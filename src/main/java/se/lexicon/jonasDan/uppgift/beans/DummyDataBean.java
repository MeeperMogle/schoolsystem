package se.lexicon.jonasDan.uppgift.beans;

import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Lesson;
import se.lexicon.jonasDan.uppgift.entities.Level;
import se.lexicon.jonasDan.uppgift.entities.Login;
import se.lexicon.jonasDan.uppgift.entities.Person;
import se.lexicon.jonasDan.uppgift.entities.Role;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-09)
 * Adds basic dummy data to the database.
 */
@RequestScoped
@Named
public class DummyDataBean {

    @Inject
    private Persistor storer;

    /**
     * Public method used to quickly add basic dummy data.
     * <br>Actual encapsulated method call inside commented out while
     * not wanting to accidentally activate it.
     */
    public void addDummyData() {
        System.out.println("Jonas deactivated");
        doDataInsertion();

    }

    /**
     * Adds dummy data to the database via Persistor class.
     */
    private void doDataInsertion() {
        // Role is the access control. Without it no users would work.
        Role administratorRole = new Role("Administrator"),
                teacherRole = new Role("Teacher"),
                studentRole = new Role("Student");
        List<Role> allRoles = Arrays.asList(
                administratorRole, teacherRole, studentRole);

        // Logins are the user credentials. They are connected to a Role.
        Login ginoLogin = new Login("gino", "gino123", studentRole),
                danielLogin = new Login("daniel", "daniel123", teacherRole),
                bengtLoginStudent = new Login("bengt1", "bengt123", studentRole),
                bengtLoginTeacher = new Login("bengt2", "bengt123", teacherRole),
                helenaLogin = new Login("helena", "helena123", administratorRole);
        List<Login> allLogins = Arrays.asList(
                ginoLogin, danielLogin, bengtLoginStudent, bengtLoginTeacher,
                helenaLogin);

        // Person can have zero or more Logins.
        Person gino = new Person("Gino", "10000000-0000", "1111111111",
                "gino@lexicon.se", "Ginoborg"),
                daniel = new Person("Daniel", "20000000-0000", "2222222222",
                        "daniel@lexicon.se", "Danielstrand"),
                bengt = new Person("Bengt", "30000000-0000", "3333333333",
                        "bengt@lexicon.se", "Bengtsfors"),
                helena = new Person("Helena", "40000000-0000", "4444444444",
                        "helena@lexicon.se", "Helenejamn"),
                benjamin = new Person("Benjamin", "50000000-0000", "5555555555",
                        "benjamin@lexicon.se", "Benjamina");
        List<Person> allPersons = Arrays.asList(
                gino, daniel, bengt, helena, benjamin);

        // All login credentials are added together
        gino.setAccountDetails(Arrays.asList(ginoLogin));
        daniel.setAccountDetails(Arrays.asList(danielLogin));
        bengt.setAccountDetails(Arrays.asList(bengtLoginStudent, bengtLoginTeacher));
        helena.setAccountDetails(Arrays.asList(helenaLogin));


        // Course levels are required to add any Courses
        Level prep = new Level("Preparatory"),
                basic = new Level("Basic"),
                advanced = new Level("Advanced");
        List<Level> allLevels = Arrays.asList(
                prep, basic, advanced);

        // Courses are dependant on Level
        Course jse = new Course("Java SE", "JSE1", prep, "Swedish", 15,
                Date.valueOf(LocalDate.of(2016, 5, 1)), daniel),
                jee = new Course("Java EE", "JEE1", advanced, "Swedish", 14,
                        Date.valueOf(LocalDate.of(2016, 7, 1)), daniel),
                jpa = new Course("JPA", "JEE-JPA", basic, "Swedish", 14,
                        Date.valueOf(LocalDate.of(2016, 7, 14)), daniel),
                jms = new Course("JMS", "JEE-JMS", basic, "Swedish", 14,
                        Date.valueOf(LocalDate.of(2016, 7, 21)), daniel),
                webb = new Course("Webb", "WWW1", prep, "English", 14,
                        Date.valueOf(LocalDate.of(2016, 6, 6)), daniel),
                jaxRs = new Course("Jax-RS", "JEE-JRS", advanced, "Swedish", 14,
                        Date.valueOf(LocalDate.of(2016, 9, 14)), daniel);
        List<Course> allCourses = Arrays.asList(
                jse, jee, jpa, jms, webb, jaxRs);

        // Lessons can be set to belong to an existing Course
        Lesson jse1 = new Lesson(jse.getStartDay(),
                Time.valueOf("09:00:00"), "Effekt", jse),
                jse2 = new Lesson(jse.getStartDay(),
                        Time.valueOf("13:00:00"), "Effekt", jse),
                jse3 = new Lesson(jse.getStartDay(),
                        Time.valueOf("14:15:00"), "Effekt", jse),
                jee1 = new Lesson(jee.getStartDay(),
                        Time.valueOf("09:13:00"), "Enterprise-lokal", jee),
                jms1 = new Lesson(jms.getStartDay(),
                        Time.valueOf("09:10:00"), "Joking Message lokal", jms),
                jaxRs1 = new Lesson(jaxRs.getStartDay(),
                        Time.valueOf("10:15:00"), "Jax-ax lokal", jaxRs),
                jaxRs2 = new Lesson(jaxRs.getStartDay(),
                        Time.valueOf("13:30:00"), "Jax-ersättningslokal", jaxRs),
                webb1 = new Lesson(webb.getStartDay(),
                        Time.valueOf("09:00:01"), "Interwebs", webb);
        List<Lesson> allLessons = Arrays.asList(
                jse1, jse2, jse3, jee1, jms1, jaxRs1, jaxRs2, webb1);

        // Add everything to the database via persistence context connector class
        allRoles.forEach(x -> storer.persistRole(x));
        allLogins.forEach(x -> storer.persistLogin(x));
        allPersons.forEach(x -> storer.persistPerson(x));
        allLevels.forEach(x -> storer.persistLevel(x));
        allCourses.forEach(x -> storer.persistCourse(x));
        allLessons.forEach(x -> storer.persistLesson(x));
    }

}
