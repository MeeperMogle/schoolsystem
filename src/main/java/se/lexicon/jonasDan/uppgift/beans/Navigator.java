package se.lexicon.jonasDan.uppgift.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * @author Jonas Olsson (2016-09-08)
 *         Common class for creating navigation strings across site.
 */
@Named
@RequestScoped
public class Navigator {
    /**
     * Generate standardised navigation link - format common for the entire site.
     *
     * @param location page name, without .xtml, relative to root (without /)
     * @return navigation string for use by JSF links etc
     */
    public static String navigate(String location) {
        return "/" + location + "?faces-redirect=true";
    }
}
