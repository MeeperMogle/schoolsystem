package se.lexicon.jonasDan.uppgift.beans;

import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author Jonas Olsson (2016-09-08)
 *         Bean for fetching the basic (common) information about
 *         a Person, based on the page ID corresponding to their ID.
 */
@SessionScoped
@Stateful
@Named
public class ShowPersonBean implements Serializable {
    private Person person;

    @Inject
    private Persistor loader;

    @Inject
    private LoginBean credentials;

    /**
     * Fetch ID from GET parameters. This default method uses "id" as the GET parameter.
     * <br>Load a Person object from the database based on the id.
     */
    public void loadGottenId() {
        loadGottenId("id");
    }

    /**
     * Fetch ID from GET parameters, load a Person object from the database based on it.
     * @param parameterName GET parameter name to use
     */
    public void loadGottenId(String parameterName) {
        // ID from the JSF page context
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get(parameterName));

            this.person = loader.getPersonById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    /**
     * Determine whether the person viewing has access to edit the profile information.
     * @param id person to attempt editing
     * @return true: administrator access, or it's yourself. false: otherwise
     */
    public boolean hasAccessToEdit(Long id) {
        try {
            return credentials.isAdministratorAccess() ||
                    credentials.getUserPerson().getId().equals(id);
        } catch (Exception e) {
            return false;
        }
    }

    public Person getPerson() {
        return person;
    }
}
