package se.lexicon.jonasDan.uppgift.beans.course;

import se.lexicon.jonasDan.uppgift.beans.Navigator;
import se.lexicon.jonasDan.uppgift.beans.Persistor;
import se.lexicon.jonasDan.uppgift.entities.Course;
import se.lexicon.jonasDan.uppgift.entities.Lesson;
import se.lexicon.jonasDan.uppgift.entities.Person;

import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * @author Jonas Olsson (2016-09-08)
 */
@SessionScoped
@Stateful
@Named
public class ShowCourseBean implements Serializable {
    private Course course;

    @Inject
    private Persistor loader;

    public void loadGottenId() {
        loadGottenId("id");
    }

    /**
     * Fetch the ID to edit from the GET parameters or,
     * if that has been lost, let it remain as it was since last time.
     */
    public void loadGottenId(String paramsIdName) {
        try {
            Long id = Long.parseLong(
                    FacesContext.getCurrentInstance().getExternalContext().
                            getRequestParameterMap().get(paramsIdName));

            this.course = loader.getCourseById(id);
        } catch (NumberFormatException | EJBException | NullPointerException e) {
            System.out.println("the error: " + e);
        }
    }

    /**
     * Check whether the given Person id belongs to a student on this Course.
     * @param studentId supposed student id
     * @return true: Person is registered as a student to this Course. false: they are not
     */
    public boolean isCourseStudent(Long studentId) {
        for (Course course : loader.getPersonById(studentId).getCourses()) {
            if (course.getId().equals(this.getCourse().getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add Person with given id as a student of this Course.
     * @param studentId id of the Person to register as a student on this course
     */
    public void addStudentToCourse(Long studentId) {
        Person student = loader.getPersonById(studentId);
        student.addCourse(this.getCourse());
        course.addStudent(student);
        loader.updatePerson(student);
        loader.updateCourse(this.getCourse());
    }

    public Course getCourse() {
        return course;
    }

    /**
     * Remove Person with given id as a student from this Course.
     * @param studentId id of the Person to de-register as a student from this course
     */
    public void removeStudentFromCurrentCourse(Long studentId) {
        Person student = loader.getPersonById(studentId);
        student.removeCourse(this.getCourse());
        course.removeStudent(student);
        loader.updatePerson(student);
        loader.updateCourse(this.getCourse());
    }

    /**
     * Remove Person with given id as a student from the given Course.
     * @param studentId id of the Person to de-register as a student
     * @param courseId id of the Course to de-register the student from
     * @return navigation string pointing to the student profile of the given Person
     */
    public String removeStudentFromCourse(Long studentId, Long courseId) {
        Person student = loader.getPersonById(studentId);
        Course course = loader.getCourseById(courseId);

        student.removeCourse(course);
        course.removeStudent(student);
        loader.updatePerson(student);
        loader.updateCourse(course);

        return Navigator.navigate("student/show") + "id=" + studentId;
    }

    public List<Lesson> getLessons() {
        return loader.loadLessonsForCourseId(this.getCourse().getId());
    }

    /**
     * Check whether the given Person can manage Lesson-list for this Course.
     * @param personId Person to check access level of
     * @return true: Person is set as the teacher of the Course. false: not set as teacher
     */
    public boolean personHasAccessToManageLessons(Long personId) {
        return this.getCourse().getTeacher().getId().equals(personId);
    }

    public List<Person> getCourseStudents() {
        return this.getCourse().getStudents();
    }
}
